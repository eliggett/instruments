# Instruments

This project serves to track and maintain vintage midi Instrument Definition files. These files are used to inform DAWs and MIDI Trackers of what banks and sounds exist on a given instrument. 

## Included here: 

This repository includes a file I created for the Roland U-110, called "U-110.ins" which contains the banks, tones, and drum map for the Roland U-110. 

I also have a file for the Kurzweil PC-88. This file includes definitions for the PC-88MX. This also means that the file includes definitions for the VGM expansion card. 

I have also included a file for the U-220 and a large file for many other Roland devices. I have not tested these but they are from a reliable source. 

## File formats:

The files are in a plain text format and can be easily edited using Notepad, Text Edit, or any other basic text editor. The format is simple and is detailed in the file "Cakewalk_Instrumentendefinitions_en.pdf". If you are creating a new instrument definition, take a similar instrument definition file and edit it to match your instrument. 

## Compatibility: 

I have tested the Roland U-110 Instrument Definition file with QTractor. It is likely that these files would work with many other DAWs. 
